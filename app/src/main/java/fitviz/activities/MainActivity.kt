package fitviz.activities

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import fitviz.fragments.GroupsFragment
import fitviz.fragments.HomeFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val homeFragment: Fragment = HomeFragment()
    private val groupsFragment: Fragment = GroupsFragment()
    private var active: Fragment = homeFragment
    private val fragmentManager = supportFragmentManager

    //    private val planFragment: Fragment = PlanFragment()

    private val mOnNavigationItemSelectedListener =
        object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.menu_home -> {
                        fragmentManager.beginTransaction().hide(active).show(homeFragment).commit()
                        active = homeFragment
                        return true
                    }
                    R.id.menu_groups -> {
                        fragmentManager.beginTransaction().hide(active).show(groupsFragment).commit()
                        active = groupsFragment
                        return true
                    }
                    R.id.menu_plan -> {
                        return true
                    }
                }
                return false
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // fragmentManager.beginTransaction().add(R.id.main_container, planFragment).hide(planFragment).commit()
        fragmentManager.beginTransaction().add(R.id.main_container, groupsFragment)
            .hide(groupsFragment).commit()
        fragmentManager.beginTransaction().add(R.id.main_container, homeFragment).show(homeFragment).commit()

        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }
}
