package fitviz.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val sharedPref = applicationContext.getSharedPreferences(
            R.string.app_preferences_file_name.toString(),
            Context.MODE_PRIVATE
        )
        val accessToken = sharedPref.getString("access_token", null)
        if(accessToken != null){
            launchMainActivity()
        }

        loginButton.setOnClickListener {
            val username = username.text
            val password = password.text
            val tokenEndpoint = "http://10.0.2.2:3000/oauth/token"

            // TODO replace this with a singleton class for request queue https://developer.android.com/training/volley/requestqueue.html
            val queue = Volley.newRequestQueue(this)

            val tokenRequestObject = JSONObject()
            tokenRequestObject.put("email", username)
            tokenRequestObject.put("password", password)
            tokenRequestObject.put("grant_type", "password")

            val tokenRequest = JsonObjectRequest(
                Request.Method.POST,
                tokenEndpoint,
                tokenRequestObject,
                Response.Listener { response ->
                    val token = response.getString("access_token")

                    //save token to app storage
                    val sharedPref = applicationContext.getSharedPreferences(
                        R.string.app_preferences_file_name.toString(),
                        Context.MODE_PRIVATE
                    )
                    val editor = sharedPref.edit()
                    editor.putString("access_token", token)
                    editor.commit()

                    //redirect to main activity
                    launchMainActivity()
                },
                Response.ErrorListener { error ->
                    //TODO display error message
                })

            queue.add(tokenRequest)
        }
    }

    private fun launchMainActivity(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}